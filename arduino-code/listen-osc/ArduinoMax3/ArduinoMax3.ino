 /*
Arduino vers MAX
Arnaud Recher
Mars 2016
 */


#include <SPI.h>         // needed for Arduino versions later than 0018
#include <Ethernet2.h>
#include <EthernetUdp2.h>         // UDP library from: bjoern@cs.stanford.edu 12/30/2008
#include <OSCMessage.h>

int sensorID = 1;
int pulsePin = 0;                 // Pulse Sensor purple wire connected to analog pin 0
int blinkPin = 13;                // pin to blink led at each beat
int fadePin = 5;                  // pin to do fancy classy fading blink at each beat
int fadeRate = 0;                 // used to fade LED on with PWM on fadePin     
int analogPin = 3;

volatile float BPM;                   // int that holds raw Analog in 0. updated every 2mS
volatile int Signal;                // holds the incoming raw data for heart pulse
volatile int IBI = 600;             // int that holds the time interval between beats! Must be seeded! 
volatile boolean Pulse = false;     // "True" when User's live heartbeat is detected. "False" when not a "live beat". 
volatile boolean QS = false;        // becomes true when Arduoino finds a beat.
volatile int AMP = 100;                   // used to hold amplitude of pulse waveform, seeded
volatile int Respiration;                // holds the incoming raw data for respiration

unsigned long previousMillis = 0;
unsigned long periode_echant = 5; //ms

// Enter a MAC address and IP address for your controller below.
// The IP address will be dependent on your local network:
byte mac[] = {
  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED
};
IPAddress ip(192, 168, 1, 170); //Arduino n°1
unsigned int localPort = 8880; // Arduino n°1 local port
//IPAddress ip(192, 168, 1, 171); //Arduino n°2
//unsigned int localPort = 8881; // Arduino n°2 local port
//IPAddress ip(192, 168, 1, 172); //Arduino n°3
//unsigned int localPort = 8882; // Arduino n°3 local port
//IPAddress ip(192, 168, 1, 173); //Arduino n°4
//unsigned int localPort = 8883; // Arduino n°4 local port
//IPAddress ip(192, 168, 1, 174); //Arduino n°5
//unsigned int localPort = 8884; // Arduino n°5 local port
//IPAddress ip(192, 168, 1, 175); //Arduino n°6
//unsigned int localPort = 8885; // Arduino n°6 local port
IPAddress destIp(192, 168, 1, 100); //Computer
unsigned int DestPort = 9990; //Computer for Arduino n°1 local port to listen on
//unsigned int DestPort = 9991; //Computer for Arduino n°2 local port to listen on
//unsigned int DestPort = 9992; //Computer for Arduino n°3 local port to listen on
//unsigned int DestPort = 9993; //Computer for Arduino n°4 local port to listen on
//unsigned int DestPort = 9994; //Computer for Arduino n°5 local port to listen on
//unsigned int DestPort = 9995; //Computer for Arduino n°6 local port to listen on

// buffers for receiving and sending data
char packetBuffer[UDP_TX_PACKET_MAX_SIZE];  //buffer to hold incoming packet,
char  ReplyBuffer[] = "acknowledged";       // a string to send back

EthernetUDP Udp;                     // An EthernetUDP instance to let us send and receive packets over UDP

OSCMessage msg("/pulse/analog");
OSCMessage msg2("/pulse/beat");
OSCMessage msg3("/breath");
OSCMessage msg4("/pulse/amp");


static boolean serialVisual = false;   // Set to 'false' by Default.  Re-set to 'true' to see Arduino Serial Monitor ASCII Visual Pulse 

void setup() 
{
  interruptSetup();                 // sets up to read Pulse Sensor signal every 2mS 
  
  Ethernet.begin(mac, ip);          // start the Ethernet and UDP:
  Udp.begin(localPort);

  Serial.begin(115200);
  Serial.println("start UDP OSC");

  pinMode(blinkPin,OUTPUT);         // pin that will blink to your heartbeat!
  pinMode(fadePin,OUTPUT);          // pin that will fade to your heartbeat!

  Serial.print("ID");
  Serial.print(sensorID);  
   //   analogReference(EXTERNAL); // IF YOU ARE POWERING The Pulse Sensor AT VOLTAGE LESS THAN THE BOARD VOLTAGE, UN-COMMENT THE NEXT LINE AND APPLY THAT VOLTAGE TO THE A-REF PIN
}

void loop() 
{
  // periode ech : 5ms
  // intégrer gestion du temps avec un if(millis()-last time > periode echant)
  if(millis()- previousMillis >= periode_echant)
  {
    previousMillis = millis();
  
    // if there's data available, read a packet
    int packetSize = Udp.parsePacket();
   if (packetSize) {
      Serial.print("Received packet of size ");
      Serial.println(packetSize);
      Serial.print("From ");
      IPAddress remote = Udp.remoteIP();
      for (int i = 0; i < 4; i++) {
        Serial.print(remote[i], DEC);
        if (i < 3) {
          Serial.print(".");
        }
      }
      Serial.print(", port ");
      Serial.println(Udp.remotePort());

    // read the packet into packetBufffer
    Udp.read(packetBuffer, packetSize); //UDP_TX_PACKET_MAX_SIZE
    Serial.println("Contents:");
    Serial.println(packetBuffer);

    // send a reply to the IP address and port that sent us the packet we received
    Udp.beginPacket(Udp.remoteIP(), DestPort);
    Udp.write(ReplyBuffer);
    Udp.endPacket();
  }

  //ledFadeToBeat();                      // Makes the LED Fade Effect Happen 
  //sendDataToSerial('R',analogRead(A1)); //  Send Breath signal
  //  take a break

  msg.add(analogRead(0));
//  msg.send(SLIPSerial); // send the bytes to the SLIP stream
//  SLIPSerial.endPacket(); // mark the end of the OSC Packet
//  msg.empty(); // free space occupied by message
  Udp.beginPacket(destIp, DestPort);
  msg.send(Udp); // send the bytes to the SLIP stream
  Udp.endPacket(); // mark the end of the OSC Packet
  msg.empty(); // free space occupied by message

 
  if (QS == true){     // A Heartbeat Was Found
                       // BPM and IBI have been Determined
                       // Quantified Self "QS" true when arduino finds a heartbeat
        digitalWrite(blinkPin,HIGH);     // Blink LED, we got a beat. 
          msg2.add(BPM);
          Udp.beginPacket(destIp, DestPort);
          msg2.send(Udp); // send the bytes to the SLIP stream
          Udp.endPacket(); // mark the end of the OSC Packet
          msg2.empty(); // free space occupied by message

          msg4.add(AMP);
          Udp.beginPacket(destIp, DestPort);
          msg4.send(Udp); // send the bytes to the SLIP stream
          Udp.endPacket(); // mark the end of the OSC Packet
          msg4.empty(); // free space occupied by message
          
        QS = false;                      // reset the Quantified Self flag for next time    
  }


  //valresp = analogRead(analogPin); 
  Serial.println(Respiration); 
  msg3.add(analogRead(Respiration));
  Udp.beginPacket(destIp, DestPort);
  msg3.send(Udp); // send the bytes to the SLIP stream
  Udp.endPacket(); // mark the end of the OSC Packet
  msg3.empty(); // free space occupied by message
}
}

void ledFadeToBeat(){
    fadeRate -= 15;                         //  set LED fade value
    fadeRate = constrain(fadeRate,0,255);   //  keep LED fade value from going into negative numbers!
    analogWrite(fadePin,fadeRate);          //  fade LED
}
